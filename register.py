from flask import Flask, url_for, send_file, request
from jinja2 import Template

from random import choices
from datetime import datetime as dt
from datetime import timedelta

import _thread
from time import sleep
from functools import lru_cache

from subprocess import run

from web_captcha import WebCaptcha


# constants
ALPHABET = "234567890" # may be longer, but needs configuration
CAPTHA_LIFETIME_SEC = 120
CAPTCHA_LENGHT = 5


# global vars
captchas = list() # order is important

app = Flask(__name__)



# process POST request
@app.route('/register/', methods=['POST'])
def process_register_post_request():

    username = request.form["username"]
    password = request.form["password"]
    confirm_password = request.form["confirm_password"]

    # list of captchas, that match text, that user inputed
    captcha_matches = list(filter(lambda x: 
                        x.text == request.form["captcha_input"],
                     captchas))

    if not captcha_matches:
        message = "Wrong or expired captcha!"
    
    elif password != confirm_password:
        message = "Password do not match!"

    elif " " in username:
        message = "Username must not include gap!"

    elif " " in password:
        message="Password must not include gap!"

    else:

        # run backend
        process = run(
                    ["./regej.sh", username, "example.com", password],
                    capture_output=True
                  )

        # remove used captcha
        captchas.remove(captcha_matches[0])

        username = ""
        password = ""
        confirm_password = ""
        message = process.stdout.decode("UTF-8").strip() + '!'

    return render_register_form(message=message, username=username, 
                             password=password, confirm_password=confirm_password)


# process GET request
@app.route('/register/', methods=['GET'])
def process_register_get_request():
    return render_register_form()


# render
def render_register_form( captcha_url=None, message="Have to fill all fields.",
                          username="", password="", confirm_password=""):
    """
    Render and return register form.
    Can generate captcha.
    """

    template = open_template_file("templates/register.html")

    # styles
    styles_url = url_for_static_file('static', filename='css/register.css')

    # generate captcha for every request
    if captcha_url is None:
        random_string = ''.join(choices(ALPHABET, k=CAPTCHA_LENGHT))
        captcha = WebCaptcha(random_string, folder="var/", endpoint="var")
        captcha_url = url_for(captcha.endpoint, filename=captcha.name)

        # captcha expiration date
        now = dt.now()
        captcha.expiration_date = now + timedelta(days=0, hours=0, minutes=0,
                                                  seconds=CAPTHA_LIFETIME_SEC)

        captchas.append(captcha)

    return template.render( captcha=captcha_url, styles=styles_url,
                            message=message, username=username, password=password,
                            confirm_password=confirm_password)



# Route for local variable files. Mostly captchas.
@app.route('/register/var/<filename>', methods=['GET', 'POST'])
def var(filename):
    return send_file(f"var/{filename}")


# needed evil
@lru_cache
def open_template_file(path):
    template = Template(open(path).read())
    return template


@lru_cache
def url_for_static_file(*args, **kvargs):
    url = url_for(*args, **kvargs)
    return url


def expired_captcha_collector(n):
    """
    every n seconds compute count of expired captchas
    delete slice captchas[:computed_count]
    """

    while True:
        now = dt.now()

        if not captchas or captchas[0].expiration_date > now:
            sleep(n)
            continue

        # count of expired captchas
        expired_captchas_slice_len = len(list(filter(lambda x: x.expiration_date <= now, 
                                                     captchas)))
        del captchas[:expired_captchas_slice_len]

        if captchas:
            delta = captchas[0].expiration_date - now

            # sleep until next captcha must expire
            sleep(delta.total_seconds())


def main():
    try:
        _thread.start_new_thread(expired_captcha_collector, (CAPTHA_LIFETIME_SEC,))
        app.run()
    except Exception as e:
        print(str(e))
    finally:

        # Look at this when your search, what deletes captchas! Look at this!
        del captchas[::]


if __name__ == '__main__':
    main()
