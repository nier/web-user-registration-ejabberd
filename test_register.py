import pytest
import register

import _thread
from time import sleep

from os import popen
import requests

def test_captcha_expiration():
    register.captcha_lifetime_sec = 1
    _thread.start_new_thread(register.main, ())

    r1 = requests.get("http://localhost:5000/register/")

    p1 = popen("ls var/captcha*.png")

    assert p1.readlines(), "captcha is not created!"
    sleep(2)

    p2 = popen("ls var/*.png")
    assert not p2.readlines(), "captchas is not deleted!"

    


