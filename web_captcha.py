from captcha.image import ImageCaptcha
import os
#from PIL import Image

class WebCaptcha:
    def __init__(self, text, folder="var/images/", endpoint="var/images/"):
        if not os.path.exists("./var"):
            os.makedirs("./var")
        
        self.text = text
        self.folder = folder
        self.endpoint = endpoint

        image = ImageCaptcha(width = 280, height = 90)
        data = image.generate(self.text)

        self.hash = hash(data)
        self.name = f'captcha_{str(self.hash)}.png'
        self.path = self.folder + self.name

        image.write(text, self.path)

        del image
        del data


    def __del__(self):
        os.remove(self.path)


if __name__ == "__main__":
    c = WebCaptcha("12345")

